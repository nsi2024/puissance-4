Protocole

Jouer en 1 contre 1 :

-Appuyez sur le bouton "Jouer � 2" pour lancer une partie en mode 1 contre 1.
-Le tour d'une des deux couleurs (rouge ou jaune) sera indiqu�.
-Pour placer un jeton, cliquez sur les boutons en haut de chaque colonne correspondant � l'emplacement o� vous souhaitez placer votre jeton.
-La partie se termine lorsqu'un joueur aligne 4 pions de sa couleur, soit verticalement, horizontalement ou en diagonale.
-� la fin de la partie, un �cran de victoire s'affiche, offrant la possibilit� de rejouer ou de quitter le jeu.

Jouer contre un robot :

-Cliquez sur le bouton "Ordinateur" pour d�marrer une partie contre l'ordinateur.
-S�lectionnez le niveau de difficult� entre facile et moyen.
-Le joueur commence la partie en pla�ant un jeton.
-Le robot joue instantan�ment apr�s le joueur.
-La partie se poursuit avec les joueurs alternant jusqu'� ce qu'un joueur aligne 4 pions ou que le plateau soit rempli.
-� la fin de la partie, un �cran de victoire s'affiche, offrant la possibilit� de rejouer ou de quitter le jeu.