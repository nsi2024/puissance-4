


########## Importer les modules necessaires ##############
from tkinter import *
from random import *
import time
from tkinter.font import Font
from random import*
##########################################################
##########    Fonctions ##################################
########################################################## 


'''Fonction pagejeu qui détruit la fenêtre principale et qui en créé une nouvelle appellée jeu. 
Sur le modèle de la variable grille, une grille est affichée de 7 par 6 avec un bouton au dessus 
de chaque colonne. A l'appuie d'un bouton, une fonction pion(numéro du bouton) est actionée'''

def initialisation():
    global grille 
    global puissance 
    global compteur 
    global a 
    global b 
    global c
    global d1
    global e
    global f
    global h
    a=1
    b=1
    c=1
    d1=1
    e=1
    f=1
    h=1
    grille= [[0,0,0,0,0,0,0],
         [0,0,0,0,0,0,0],
         [0,0,0,0,0,0,0],
         [0,0,0,0,0,0,0],
         [0,0,0,0,0,0,0],
         [0,0,0,0,0,0,0]]
    compteur=0

def iapage():
    global ia 
    ia = Tk()
    
    Canevas = Canvas(ia,width=1000,height=800,bg ='#C8AD7F')
    Canevas.pack()
    bouton=Button(ia, text="FACILE", command=ia1,bg='red',fg='yellow',width=30,height=5)
    bouton.place(x=100,y=400)
    bouton=Button(ia, text="MOYEN", command=ia2,bg='red',fg='yellow',width=30,height=5)
    bouton.place(x=400,y=400)
    bouton=Button(ia, text="DIFFICLE", command=ia3,bg='red',fg='yellow',width=30,height=5)
    bouton.place(x=700,y=400)
    
    ttt=Canevas.create_text(500,300,text="Difficulté de l'ordi :" ,fill="Black", font=("Calibri", 30)) 
    
    

def ia1():
    global n
    n=1
    ia.destroy()
    pagejeu()
    
def ia2():
    global n
    n=2
    ia.destroy()
    pagejeu()
    
def ia3():
    global n
    n=3
    ia.destroy()
    pagejeu()
    
def coup1():
    nombre=randint(1,7)
    if nombre==1 and grille[0][0]==0:
        pionia1()
    elif nombre==2 and grille[0][1]==0:
        pionia2()
    elif nombre==3 and grille[0][2]==0:
        pionia3()
    elif nombre==4 and grille[0][3]==0:
        pionia4()
    elif nombre==5 and grille[0][4]==0:
        pionia5()
    elif nombre==6 and grille[0][5]==0:
        pionia6()
    elif nombre==7 and grille[0][6]==0:
        pionia7()
    else: coup1()
        
        
def coup2():
    global u
    
    for i in range(3):
       
        #verif verticale bleue
        
        if grille[5-i][0]==1 and grille[4-i][0]==1 and grille[3-i][0]==1 and grille[2-i][0]==0:
        
            u=1
            pionia1()
            
            return()
        
        
        elif grille[5-i][1]==1 and grille[4-i][1]==1  and grille[3-i][1]==1 and grille[2-i][1]==0:
            u=2
            pionia2()
            
            return()
       
   
        elif grille[5-i][2]==1 and grille[4-i][2]==1  and grille[3-i][2]==1 and grille[2-i][2]==0:
            u=3
            pionia3()
            
            return()
        
        elif grille[5-i][3]==1 and grille[4-i][3]==1  and grille[3-i][3]==1 and grille[2-i][3]==0:
            u=4 
            pionia4()
            
            return()
       
       
 
        elif grille[5-i][4]==1 and grille[4-i][4]==1  and grille[3-i][4]==1 and grille[2-i][4]==0:
            u=5
            pionia5()
            
            return()
       

        elif grille[5-i][5]==1 and grille[4-i][5]==1  and grille[3-i][5]==1 and grille[2-i][5]==0:
            u=6
            pionia6()
            
            return()
      
       
  
        elif grille[5-i][6]==1 and grille[4-i][6]==1  and grille[3-i][6]==1 and grille[2-i][6]==0:
            u=7
            pionia7()
            
            return()
        
        #verif verticale rouge
        
        elif grille[5-i][0]==2 and grille[4-i][0]==2  and grille[3-i][0]==2 and grille[2-i][0]==0:
            u=1
            pionia1()
            
            return()
        
        
        elif grille[5-i][1]==2 and grille[4-i][1]==2  and grille[3-i][1]==2 and grille[2-i][1]==0:
            
            u=2
            pionia2()
            
            return()
       
   
        elif grille[5-i][2]==2 and grille[4-i][2]==2  and grille[3-i][2]==2 and grille[2-i][2]==0:
            u=3
            pionia3()
            
            return()
        
        elif grille[5-i][3]==2 and grille[4-i][3]==2  and grille[3-i][3]==2 and grille[2-i][3]==0:
            u=4
            pionia4()
            
            return()
       
       
 
        elif grille[5-i][4]==2 and grille[4-i][4]==2 and grille[3-i][4]==2 and grille[2-i][4]==0:
            u=5
            pionia5()
            
            return()
       

        elif grille[5-i][5]==2 and grille[4-i][5]==2  and grille[3-i][5]==2 and grille[2-i][5]==0:
            u=6
            pionia6()
           
            return()
      
       
  
        elif grille[5-i][6]==2 and grille[4-i][6]==2  and grille[3-i][6]==2 and grille[2-i][6]==0:
            u=7
            pionia7()
            
            return()
        
        
        
        
        #verif diagonale bas a haut gauche a droit bleue
        
        elif grille[5-i][0]==1 and grille[4-i][1]==1 and grille[3-i][2]==1 and grille[2-i][3]==0 and grille[3-i][3]!=0:
            pionia4()
            return()
        
        elif grille[5-i][1]==1 and grille[4-i][2]==1 and grille[3-i][3]==1 and grille[2-i][4]==0 and grille[3-i][4]!=0:
            pionia5()
            return()
        
        elif grille[5-i][2]==1 and grille[4-i][3]==1 and grille[3-i][4]==1 and grille[2-i][5]==0 and grille[3-i][5]!=0:
            pionia6()
            return()
        
        elif grille[5-i][3]==1 and grille[4-i][4]==1 and grille[3-i][5]==1 and grille[2-i][6]==0 and grille[3-i][6]!=0:
            pionia7()
            return()
        
        #verif diagonale bas a haut droit a gauche bleue
        
        
        elif grille[5-i][6]==1 and grille[4-i][5]==1 and grille[3-i][4]==1 and grille[2-i][3]==0 and grille[3-i][3]!=0:
            pionia4()
            return()
        
        elif grille[5-i][5]==1 and grille[4-i][4]==1 and grille[3-i][3]==1 and grille[2-i][2]==0 and grille[3-i][2]!=0:
            pionia3()
            return()
        
        elif grille[5-i][4]==1 and grille[4-i][3]==1 and grille[3-i][2]==1 and grille[2-i][1]==0 and grille[3-i][1]!=0:
            pionia2()
            return()
        
        elif grille[5-i][3]==1 and grille[4-i][2]==1 and grille[3-i][1]==1 and grille[2-i][0]==0 and grille[3-i][0]!=0:
            pionia1()
            return()
        
        #verif diagonale haut a bas droite a gauche bleue( avec un pion sur la ligne du bas)
        
        elif grille[2][6]==1 and grille[3][5]==1 and grille[4][4]==1 and grille [5][3]==0:
            pionia4()
            return()
        
        elif grille[2][5]==1 and grille[3][4]==1 and grille[4][3]==1 and grille [5][2]==0:
            pionia3()
            return()
        
        elif grille[2][4]==1 and grille[3][3]==1 and grille[4][2]==1 and grille [5][1]==0:
            pionia2()
            return()
        
        elif grille[2][3]==1 and grille[3][2]==1 and grille[4][1]==1 and grille [5][0]==0:
            pionia1()
            return()
        
        #verif diagonale haut a bas gauche a droite bleue( avec un pion sur la ligne du bas)
        
        elif grille[2][0]==1 and grille[3][1]==1 and grille[4][2]==1 and grille [5][3]==0:
            pionia4()
            return()
        
        elif grille[2][1]==1 and grille[3][2]==1 and grille[4][3]==1 and grille [5][4]==0:
            pionia3()
            return()
        
        elif grille[2][2]==1 and grille[3][3]==1 and grille[4][4]==1 and grille [5][5]==0:
            pionia2()
            return()
        
        elif grille[2][3]==1 and grille[3][4]==1 and grille[4][5]==1 and grille [5][6]==0:
            pionia1()
            return()
        
        
        
        
        #verif diagonale bas a haut gauche a droit rogue
        
        elif grille[5-i][0]==2 and grille[4-i][1]==2 and grille[3-i][2]==2 and grille[2-i][3]==0 and grille[3-i][3]!=0:
            pionia4()
            return()
        
        elif grille[5-i][1]==2 and grille[4-i][2]==2 and grille[3-i][3]==2 and grille[2-i][4]==0 and grille[3-i][4]!=0:
            pionia5()
            return()
        
        elif grille[5-i][2]==2 and grille[4-i][3]==2 and grille[3-i][4]==2 and grille[2-i][5]==0 and grille[3-i][5]!=0:
            pionia6()
            return()
        
        elif grille[5-i][3]==2 and grille[4-i][4]==2 and grille[3-i][5]==2 and grille[2-i][6]==0 and grille[3-i][6]!=0:
            pionia7()
            return()
        
        #verif diagonale bas a haut droit a gauche rouge
        
        
        elif grille[5-i][6]==2 and grille[4-i][5]==2 and grille[3-i][4]==2 and grille[2-i][3]==0 and grille[3-i][3]!=0:
            pionia4()
            return()
        
        elif grille[5-i][5]==2 and grille[4-i][4]==2 and grille[3-i][3]==2 and grille[2-i][2]==0 and grille[3-i][2]!=0:
            pionia3()
            return()
        
        elif grille[5-i][4]==2 and grille[4-i][3]==2 and grille[3-i][2]==2 and grille[2-i][1]==0 and grille[3-i][1]!=0:
            pionia2()
            return()
        
        elif grille[5-i][3]==2 and grille[4-i][2]==2 and grille[3-i][1]==2 and grille[2-i][0]==0 and grille[3-i][0]!=0:
            pionia1()
            return()
        
        #verif diagonale haut a bas droite a gauche rouge( avec un pion sur la ligne du bas)
        
        elif grille[2][6]==2 and grille[3][5]==2 and grille[4][4]==2 and grille [5][3]==0:
            pionia4()
            return()
        
        elif grille[2][5]==2 and grille[3][4]==2 and grille[4][3]==2 and grille [5][2]==0:
            pionia3()
            return()
        
        elif grille[2][4]==2 and grille[3][3]==2 and grille[4][2]==2 and grille [5][1]==0:
            pionia2()
            return()
        
        elif grille[2][3]==2 and grille[3][2]==2 and grille[4][1]==2 and grille [5][0]==0:
            pionia1()
            return()
        
        #verif diagonale haut a bas gauche a droite rouge( avec un pion sur la ligne du bas)
        
        elif grille[2][0]==2 and grille[3][1]==2 and grille[4][2]==2 and grille [5][3]==0:
            pionia4()
            return()
        
        elif grille[2][1]==2 and grille[3][2]==2 and grille[4][3]==2 and grille [5][4]==0:
            pionia3()
            return()
        
        elif grille[2][2]==2 and grille[3][3]==2 and grille[4][4]==2 and grille [5][5]==0:
            pionia2()
            return()
        
        elif grille[2][3]==2 and grille[3][4]==2 and grille[4][5]==2 and grille [5][6]==0:
            pionia1()
            return()
        
        
        
        
        
        
        
        for x in range(2):
            
            #verif diagonale haut a bas droite a gauche bleue( sans pion sur la ligne du bas)
            
            if grille[0+x][6]==1 and grille[1+x][5]==1 and grille[2+x][4]==1 and grille [3+x][3]==0 and grille[4+x][3]!=0:
                pionia4()
                return()
            
            elif grille[0+x][5]==1 and grille[1+x][4]==1 and grille[2+x][3]==1 and grille [3+x][2]==0 and grille[4+x][2]!=0:
                pionia5()
                return()
            
            elif grille[0+x][4]==1 and grille[1+x][3]==1 and grille[2+x][2]==1 and grille [3+x][1]==0 and grille[4+x][1]!=0:
                pionia6()
                return()
            
            elif grille[0+x][3]==1 and grille[1+x][2]==1 and grille[2+x][1]==1 and grille [3+x][0]==0 and grille[4+x][0]!=0:
                pionia7()
                return()
            
            #verif diagonale haut a bas gauche a droite bleue( sans pion sur la ligne du bas)
            
            elif grille[0+x][0]==1 and grille[1+x][1]==1 and grille[2+x][2]==1 and grille [3+x][3]==0 and grille[4+x][3]!=0:
                pionia4()
                return()
            
            elif grille[0+x][1]==1 and grille[1+x][2]==1 and grille[2+x][3]==1 and grille [3+x][4]==0 and grille[4+x][4]!=0:
                pionia3()
                return()
            
            elif grille[0+x][2]==1 and grille[1+x][3]==1 and grille[2+x][4]==1 and grille [3+x][5]==0 and grille[4+x][5]!=0:
                pionia2()
                return()
            
            elif grille[0+x][3]==1 and grille[1+x][4]==1 and grille[2+x][5]==1 and grille [3+x][6]==0 and grille[4+x][6]!=0:
                pionia1()
                return()
            
            
            #verif diagonale haut a bas droite a gauche rouge ( sans pion sur la ligne du bas)
            
            if grille[0+x][6]==2 and grille[1+x][5]==2 and grille[2+x][4]==2 and grille [3+x][3]==0 and grille[4+x][3]!=0:
                pionia4()
                return()
            
            elif grille[0+x][5]==2 and grille[1+x][4]==2 and grille[2+x][3]==2 and grille [3+x][2]==0 and grille[4+x][2]!=0:
                pionia5()
                return()
            
            elif grille[0+x][4]==2 and grille[1+x][3]==2 and grille[2+x][2]==2 and grille [3+x][1]==0 and grille[4+x][1]!=0:
                pionia6()
                return()
            
            elif grille[0+x][3]==2 and grille[1+x][2]==2 and grille[2+x][1]==2 and grille [3+x][0]==0 and grille[4+x][0]!=0:
                pionia7()
                return()
            
            
            #verif diagonale haut a bas gauche a droite rouge( sans pion sur la ligne du bas)
            
            elif grille[0+x][0]==2 and grille[1+x][1]==2 and grille[2+x][2]==2 and grille [3+x][3]==0 and grille[4+x][3]!=0:
                pionia4()
                return()
            
            elif grille[0+x][1]==2 and grille[1+x][2]==2 and grille[2+x][3]==2 and grille [3+x][4]==0 and grille[4+x][4]!=0:
                pionia3()
                return()
            
            elif grille[0+x][2]==2 and grille[1+x][3]==2 and grille[2+x][4]==2 and grille [3+x][5]==0 and grille[4+x][5]!=0:
                pionia2()
                return()
            
            elif grille[0+x][3]==2 and grille[1+x][4]==2 and grille[2+x][5]==2 and grille [3+x][6]==0 and grille[4+x][6]!=0:
                pionia1()
                return()
            
            
        
        
            
        
        
   
        
   
        
        
        
        for j in range(6):
            
            if grille[5][0]==1 and grille[5][1]==1 and grille[5][2]==1 and grille[5][3]==0:
                pionia4()
                return()
            
            elif grille[5][1]==1 and grille[5][2]==1 and grille[5][3]==1 and grille[5][4]==0:
                pionia5()
                return()
            
            elif grille[5][2]==1 and grille[5][3]==1 and grille[5][4]==1 and grille[5][5]==0:
                pionia6()
                return()
            
            elif grille[5][3]==1 and grille[5][4]==1 and grille[5][5]==1 and grille[5][6]==0:
                pionia7()
                return()
            
    
            elif grille[5][6]==1 and grille[5][5]==1 and grille[5][4]==1 and grille[5][3]==0:
                pionia4()
                return()
            
            elif grille[5][5]==1 and grille[5][4]==1 and grille[5][3]==1 and grille[5][2]==0:
                pionia3()
                return()
            
            elif grille[5][4]==1 and grille[5][3]==1 and grille[5][2]==1 and grille[5][1]==0:
                pionia2()
                return()
            
            elif grille[5][3]==1 and grille[5][2]==1 and grille[5][1]==1 and grille[5][0]==0:
                pionia1()
                return()
            
            
            elif grille[4-j][0]==1 and grille[4-j][1]==1 and grille[4-j][2]==1 and grille[4-j][3]==0 and grille[5-j][3]!=0:
                pionia4()
                return()
            
            elif grille[4-j][1]==1 and grille[4-j][2]==1 and grille[4-j][3]==1 and grille[4-j][4]==0 and grille[5-j][4]!=0:
                pionia5()
                return()
            
            elif grille[4-j][2]==1 and grille[4-j][3]==1 and grille[4-j][4]==1 and grille[4-j][5]==0 and grille[5-j][5]!=0:
                pionia6()
                return()
            
            elif grille[4-j][3]==1 and grille[4-j][4]==1 and grille[4-j][5]==1 and grille[4-j][6]==0 and grille[5-j][6]!=0:
                pionia7()
                return()
            
            
            elif grille[4-j][6]==1 and grille[4-j][5]==1 and grille[4-j][4]==1 and grille[4-j][3]==0 and grille[5-j][3]!=0:
                pionia4()
                return()
            
            elif grille[4-j][5]==1 and grille[4-j][4]==1 and grille[4-j][3]==1 and grille[4-j][2]==0 and grille[5-j][2]!=0:
                pionia5()
                return()
            
            elif grille[4-j][4]==1 and grille[4-j][3]==1 and grille[4-j][2]==1 and grille[4-j][1]==0 and grille[5-j][1]!=0:
                pionia6()
                return()
            
            elif grille[4-j][3]==1 and grille[4-j][2]==1 and grille[4-j][1]==1 and grille[4-j][0]==0 and grille[5-j][0]!=0:
                pionia7()
                return()
            
            
            
            elif grille[5][0]==2 and grille[5][1]==2 and grille[5][2]==2 and grille[5][3]==0:
                pionia4()
                return()
            
            elif grille[5][1]==2 and grille[5][2]==2 and grille[5][3]==2 and grille[5][4]==0:
                pionia5()
                return()
            
            elif grille[5][2]==2 and grille[5][3]==2 and grille[5][4]==2 and grille[5][5]==0:
                pionia6()
                return()
            
            elif grille[5][3]==2 and grille[5][4]==2 and grille[5][5]==2 and grille[5][6]==0:
                pionia7()
                return()
            
    
            elif grille[5][6]==2 and grille[5][5]==2 and grille[5][4]==2 and grille[5][3]==0:
                pionia4()
                return()
            
            elif grille[5][5]==2 and grille[5][4]==2 and grille[5][3]==2 and grille[5][2]==0:
                pionia3()
                return()
            
            elif grille[5][4]==2 and grille[5][3]==2 and grille[5][2]==2 and grille[5][1]==0:
                pionia2()
                return()
            
            elif grille[5][3]==2 and grille[5][2]==2 and grille[5][1]==2 and grille[5][0]==0:
                pionia1()
                return()
            
            
            elif grille[4-j][0]==2 and grille[4-j][1]==2 and grille[4-j][2]==2 and grille[4-j][3]==0 and grille[5-j][3]!=0:
                pionia4()
                return()
            
            elif grille[4-j][1]==2 and grille[4-j][2]==2 and grille[4-j][3]==2 and grille[4-j][4]==0 and grille[5-j][4]!=0:
                pionia5()
                return()
            
            elif grille[4-j][2]==2 and grille[4-j][3]==2 and grille[4-j][4]==2 and grille[4-j][5]==0 and grille[5-j][5]!=0:
                pionia6()
                return()
            
            elif grille[4-j][3]==2 and grille[4-j][4]==2 and grille[4-j][5]==2 and grille[4-j][6]==0 and grille[5-j][6]!=0:
                pionia7()
                return()
            
            
            elif grille[4-j][6]==2 and grille[4-j][5]==2 and grille[4-j][4]==2 and grille[4-j][3]==0 and grille[5-j][3]!=0:
                pionia4()
                return()
            
            elif grille[4-j][5]==2 and grille[4-j][4]==2 and grille[4-j][3]==2 and grille[4-j][2]==0 and grille[5-j][2]!=0:
                pionia5()
                return()
            
            elif grille[4-j][4]==2 and grille[4-j][3]==2 and grille[4-j][2]==2 and grille[4-j][1]==0 and grille[5-j][1]!=0:
                pionia6()
                return()
            
            elif grille[4-j][3]==2 and grille[4-j][2]==2 and grille[4-j][1]==2 and grille[4-j][0]==0 and grille[5-j][0]!=0:
                pionia7()
                return()
            
            
            
            '''elif grille[5-i][0]==1 and grille[4-i][1]==1 and grille[3-i][2]==1 and grille[2-i][3]==0 and grille[3-i][3]!=0:
                pionia4()
                return()
            
            elif grille[5-i][1]==1 and grille[4-i][2]==1 and grille[3-i][3]==1 and grille[2-i][4]==0 and grille[3-i][4]!=0:
                pionia5()
                return()
            
            elif grille[5-i][2]==1 and grille[4-i][3]==1 and grille[3-i][4]==1 and grille[2-i][5]==0 and grille[3-i][5]!=0:
                pionia6()
                return()
            
            elif grille[5-i][3]==1 and grille[4-i][4]==1 and grille[3-i][5]==1 and grille[2-i][6]==0 and grille[3-i][6]!=0:
                pionia7()
                return()
            
            
            elif grille[5-i][6]==1 and grille[4-i][5]==1 and grille[3-i][4]==1 and grille[2-i][3]==0 and grille[3-i][3]!=0:
                pionia4()
                return()
            
            elif grille[5-i][5]==1 and grille[4-i][4]==1 and grille[3-i][3]==1 and grille[2-i][2]==0 and grille[3-i][2]!=0:
                pionia3()
                return()
            
            elif grille[5-i][4]==1 and grille[4-i][3]==1 and grille[3-i][2]==1 and grille[2-i][1]==0 and grille[3-i][1]!=0:
                pionia2()
                return()
            
            elif grille[5-i][3]==1 and grille[4-i][2]==1 and grille[3-i][1]==1 and grille[2-i][0]==0 and grille[3-i][0]!=0:
                pionia1()
                return()'''
            
            
            
            
            
           
            
        
        
        
        
        

    coup1()
    return()
        
    
        
        

    
  

    

def pagejeu():

        global jeu
        global vic
        if compteur==0:
            Mafenetre.destroy()
        elif compteur==1:
            vic.destroy()
        jeu = Tk()
        
        global Canevas 
        global texte

        initialisation()
        
        Canevas = Canvas(jeu,width=1500,height=1000,bg ='grey')
        Canevas.pack()
        carree = grille
        jetons = grille
        x=200
        y=250
        d=7  
        bouton=Button(jeu, text="1", command=pion1,width=4,height=2)
        bouton.place(x=30+(x+100*0),y=y-50)
        bouton=Button(jeu, text="2", command=pion2,width=4,height=2)
        bouton.place(x=30+(x+100*1),y=y-50)
        bouton=Button(jeu, text="3", command=pion3,width=4,height=2)
        bouton.place(x=30+(x+100*2),y=y-50)
        bouton=Button(jeu, text="4", command=pion4,width=4,height=2)
        bouton.place(x=30+(x+100*3),y=y-50)
        bouton=Button(jeu, text="5", command=pion5,width=4,height=2)
        bouton.place(x=30+(x+100*4),y=y-50)
        bouton=Button(jeu, text="6", command=pion6,width=4,height=2)
        bouton.place(x=30+(x+100*5),y=y-50)
        bouton=Button(jeu, text="7", command=pion7,width=4,height=2)
        bouton.place(x=30+(x+100*6),y=y-50)
        Mapolice = Font(family='Liberation Serif', size=20) # création d'une police pour l'affichage du texte
        texte=Canevas.create_text(1150,500,text="tour du joueur",fill="black",font=Mapolice) 
        
        for i in range(6):
            for j in range(7):             
                Canevas.create_rectangle(x+(j * 100), y+(i * 100), x+(j * 100 + 100), y+(i * 100 + 100))
                Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
        if couleur==1:
            Canevas.create_oval(1250,475,1300,525,fill='navy')
        else:
            Canevas.create_oval(1250,475,1300,525,fill='red')
                    


'''fonction du bouton 1, elle parcourt la variable grille, et dessine un pion de la couleur 
défini la variable couleur ''' 
def pion1():
    
    
    
  
    global grille 
    global a
    global T1
    global couleur
    
    
    if grille[0][0]!=0:
        print("")
    else :
        if couleur==1:
            Canevas.create_oval(1250,475,1300,525,fill='red')
        else:
            Canevas.create_oval(1250,475,1300,525,fill='navy')
        if grille[5][0]==0:
            grille[5][0]=couleur
            a-=1
        else:
            grille[5-a][0]=couleur
        jetons = grille
        x=200
        y=250
        d=7  
        for i in range(6):
            for j in range(7):
                
                
                if jetons[i][j]==0:
                    Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                elif jetons[i][j]==1:
                    Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                elif jetons[i][j]==2:
                    Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
        a+=1
        if couleur==1:
           
            couleur=2
        else:
            couleur=1
        
        
   
        victoire()
        fenetre()
        if n==1 and puissance==0 :
            coup1()
        elif n==2 and puissance==0:
            coup2()
        
def pionia1():
    
    
    
  
    global grille 
    global a
    global T1
    global couleur
    
    
    if grille[0][0]!=0:
        print("")
    else :
        if couleur==1:
            Canevas.create_oval(1250,475,1300,525,fill='red')  
        else:
            Canevas.create_oval(1250,475,1300,525,fill='navy')
        if grille[5][0]==0:
            grille[5][0]=couleur
            a-=1
        else:
            grille[5-a][0]=couleur
        jetons = grille
        x=200
        y=250
        d=7  
        for i in range(6):
            for j in range(7):
                
                
                if jetons[i][j]==0:
                    Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                elif jetons[i][j]==1:
                    Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                elif jetons[i][j]==2:
                    Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
        a+=1
        if couleur==1:
           
            couleur=2
        else:
            couleur=1
        
        
   
        victoire()
        fenetre()
        
        
    
def pion2():
    
    
    global grille 
    global T1
    global b
    global couleur
    if grille[0][1]!=0:
        print("")
    else:
       if couleur==1:
           Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][1]==0:
           grille[5][1]=couleur
           b-=1
       else:
           grille[5-b][1]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
    b+=1
    if couleur==1:
        
        couleur=2
    else :
        couleur=1
     
    
    
    victoire()
    fenetre()
    if n==1 and puissance==0:
        coup1()
    elif n==2 and puissance==0:
        coup2()
    

def pionia2():
    
    
    global grille 
    global T1
    global b
    global couleur
    if grille[0][1]!=0:
        print("")
    else:
       if couleur==1:
           Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][1]==0:
           grille[5][1]=couleur
           b-=1
       else:
           grille[5-b][1]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
    b+=1
    if couleur==1:
        
        couleur=2
    else :
        couleur=1
     
    
    
    victoire()
    fenetre()
    
def pion3():
    
    global grille 
    global c
    global T1
    global couleur
    if grille[0][2]!=0:
        print("")
    else:
       if couleur==1:
               Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][2]==0:
           grille[5][2]=couleur
           c-=1
       else:
           grille[5-c][2]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
       c+=1
       if couleur==1:
          
           couleur=2
       else :
           couleur=1
           

       
       
       victoire()
       fenetre()
       if n==1 and puissance==0:
           coup1()
    
       elif n==2 and puissance==0:
           coup2()
    
def pionia3():
    
    global grille 
    global c
    global T1
    global couleur
    if grille[0][2]!=0:
        print("")
    else:
       if couleur==1:
               Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][2]==0:
           grille[5][2]=couleur
           c-=1
       else:
           grille[5-c][2]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
       c+=1
       if couleur==1:
          
           couleur=2
       else :
           couleur=1
           

       
      
       victoire()
       fenetre()
        
def pion4():
    
    global grille 
    global d1
    global T1
    global couleur
   
    if grille[0][3]!=0:
        print("")
    else:
       if couleur==1:
               Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][3]==0:
           grille[5][3]=couleur
           d1-=1
       else:
           grille[5-d1][3]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
       d1+=1
       if couleur==1:
           
           couleur=2
       else :
           couleur=1
           
           
        
       victoire()
       fenetre()
       if n==1 and puissance==0:
           coup1()
    
       elif n==2 and puissance==0:
           coup2()
def pionia4():
    
    global grille 
    global d1
    global T1
    global couleur
    if grille[0][3]!=0:
        print("")
    else:
       if couleur==1:
               Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][3]==0:
           grille[5][3]=couleur
           d1-=1
       else:
           grille[5-d1][3]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
       d1+=1
       if couleur==1:
           
           couleur=2
       else :
           couleur=1
           
           
         
       victoire()
       fenetre()
        
def pion5():
    
    global grille 
    global e
    global T1
    global couleur
    
    if grille[0][4]!=0:
        print("")
    else:
       if couleur==1:
               Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][4]==0:
           grille[5][4]=couleur
           e-=1
       else:
           grille[5-e][4]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
       e+=1
       if couleur==1:
           
           couleur=2
       else :
           
           couleur=1
           
        
       victoire()
       fenetre()
       if n==1 and puissance==0:
           coup1()  
           
    
       elif n==2 and puissance==0:
           coup2()
       
def pionia5():
    
    global grille 
    global e
    global T1
    global couleur
    
    if grille[0][4]!=0:
        print("")
    else:
       if couleur==1:
               Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][4]==0:
           grille[5][4]=couleur
           e-=1
       else:
           grille[5-e][4]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
       e+=1
       if couleur==1:
           
           couleur=2
       else :
           
           couleur=1
       victoire()
       fenetre()
       
       
       
def pion6():
    
    global grille 
    global f
    global T1
    global couleur
    if grille[0][5]!=0:
        print("")
    else:
        
       if couleur==1:
               Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][5]==0:
           grille[5][5]=couleur
           f-=1
       else:
           grille[5-f][5]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
       f+=1
       if couleur==1:
           
           couleur=2
       else :
           couleur=1
           
           
           
          
       victoire()
       fenetre()
       if n==1 and puissance==0:
           coup1()
           
        
       elif n==2 and puissance==0:
           coup2()

def pionia6():
    
    global grille 
    global f
    global T1
    global couleur
    if grille[0][5]!=0:
        print("")
    else:
        
       if couleur==1:
               Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][5]==0:
           grille[5][5]=couleur
           f-=1
       else:
           grille[5-f][5]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
       f+=1
       if couleur==1:
           
           couleur=2
       else :
           couleur=1
           
       victoire()
       fenetre()
       
       
def pion7():
    
    global grille 
    global h
    global T1
    global couleur
    
    
    if grille[0][6]!=0:
        print("")
    else:
        
       if couleur==1:
               Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][6]==0:
           grille[5][6]=couleur
           h-=1
       else:
           grille[5-h][6]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
       h+=1
       if couleur==1:
          
           couleur=2
           
       else :
           couleur=1
           
       
       victoire()
       fenetre()
       if n==1 and puissance==0:
           coup1()
           
       elif n==2 and puissance==0:
           coup2()


def pionia7():
    
    global grille 
    global h
    global T1
    global couleur
    if grille[0][6]!=0:
        print("")
    else:
        
       if couleur==1:
               Canevas.create_oval(1250,475,1300,525,fill='red')
       else:
           Canevas.create_oval(1250,475,1300,525,fill='navy')
    

    
       if grille[5][6]==0:
           grille[5][6]=couleur
           h-=1
       else:
           grille[5-h][6]=couleur
            
       jetons = grille
       x=200
       y=250
       d=7  
       for i in range(6):
               for j in range(7):
                
                
                   if jetons[i][j]==0:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='white')
                    
                   elif jetons[i][j]==1:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='navy')
                    
                   elif jetons[i][j]==2:
                       Canevas.create_oval(x+(j * 100 + d), y+(i * 100 + d),x+( j * 100 + 100 - d),y+( i * 100 + 100 - d), fill='red')
    
       h+=1
       if couleur==1:
          
           couleur=2
           
       else :
           couleur=1
       victoire()
       fenetre()
           
def victoire():

    global puissance
    global egal
    puissance=0
    egal=0
    
    for j in range(6):
        for i in range(4):
            if grille[5-j][i]==1 and grille[5-j][i+1]==1 and grille[5-j][i+2]==1 and grille[5-j][i+3]==1 :
                puissance+=1
                
    for j in range(3):
        for i in range(7):
            if grille[j][6-i]==1 and grille[j+1][6-i]==1 and grille[j+2][6-i]==1 and grille[j+3][6-i]==1 :
                puissance+=1
                
    for j in range(6):
        for i in range(4):
            if grille[5-j][i]==1 and grille[4-j][i+1]==1 and grille[3-j][i+2]==1 and grille[2-j][i+3]==1 :
                puissance+=1
            
    for j in range(3):
        for i in range(7):
            if grille[5-j][6-i]==1 and grille[4-j][5-i]==1 and grille[3-j][4-i]==1 and grille[2-j][3-i]==1 :
                puissance+=1
    for j in range(6):
        for i in range(4):
            if grille[5-j][i]==2 and grille[5-j][i+1]==2 and grille[5-j][i+2]==2 and grille[5-j][i+3]==2 :
                puissance+=2
                
    for j in range(3):
        for i in range(7):
            if grille[j][6-i]==2 and grille[j+1][6-i]==2 and grille[j+2][6-i]==2 and grille[j+3][6-i]==2 :
                puissance+=2
                
    for j in range(6):
        for i in range(4):
            if grille[5-j][i]==2 and grille[4-j][i+1]==2 and grille[3-j][i+2]==2 and grille[2-j][i+3]==2 :
                puissance+=2
            
    for j in range(3):
        for i in range(7):
            if grille[5-j][6-i]==2 and grille[4-j][5-i]==2 and grille[3-j][4-i]==2 and grille[2-j][3-i]==2 :
                puissance+=2
                
    for j in range(6):
        for i in range(7):
            if grille[j][i]!=0:
                egal+=1
        
    
    
    
            
           
        
def fenetre():
    global compteur
    global puissance
    global texte
    global Canevas
    global vic 
    global couleur
    global jeu
    compteur=1
    if puissance!=0 or egal==42:
        jeu.destroy()
        vic = Tk()
        Mapolice2 = Font(family='Liberation Serif', size=50)
        Canevas = Canvas(vic,width=1500,height=1000,bg ='grey')
        Canevas.pack()
        
        if egal == 42:
            Canevas.create_text(700,300,text="egalité" ,fill="black",font=Mapolice2)
        else:
            if couleur==2:
                Canevas.create_text(700,300,text="Victoire du joueur Bleu" ,fill="navy",font=Mapolice2) 
                Canevas.create_oval(1050,275,1100,325,fill='navy')
            
            else :
                Canevas.create_text(700,300,text="Victoire du joueur Rouge" ,fill="red",font=Mapolice2) 
                Canevas.create_oval(1050,275,1100,325,fill='red')
            
        bouton=Button(vic, text="RECOMMENCER", command=pagejeu,bg='red',fg='yellow',width=40,height=5)
        bouton.place(x=550,y=500)
        bouton=Button(vic, text="QUITTER", command=quitter,bg='red',fg='yellow',width=30,height=5)
        bouton.place(x=300,y=500)
        bouton=Button(vic, text="OPTIONS", command=createoption,bg='red',fg='yellow',width=30,height=5)
        bouton.place(x=900,y=500)
    
    
    

def quitter():
    global vic
    vic.destroy()
    
def regle():
    global fenetreregle
    fenetreregle = Tk()
    Canevas = Canvas(fenetreregle,width=1200,height=800,bg='grey')
    Canevas.pack()
    Canevas.create_text(550,100,text="Règle du jeu" ,fill='black',font=('Liberation Serif',50))
    Canevas.create_text(550,300,text="Le vainqueur est le joueur qui réalise le premier un alignement (horizontal, vertical ou diagonal)" ,fill='black',font=('Liberation Serif',20))
    Canevas.create_text(550,350,text="consécutif d'au moins quatre pions de sa couleur." ,fill='black',font=('Liberation Serif',20))
    Canevas.create_text(550,450,text="Si, alors que toutes les cases de la grille de jeu sont remplies," ,fill='black',font=('Liberation Serif',20))
    Canevas.create_text(550,500,text="aucun des deux joueurs n'a réalisé un tel alignement, la partie est déclarée nulle." ,fill='black',font=('Liberation Serif',20))
    bouton=Button(fenetreregle, text="QUITTER", command=quitter2,bg='red',fg='yellow',width=40,height=5)
    bouton.place(x=750,y=600)
    
    
def quitter2():
    global fenetreregle
    fenetreregle.destroy()
          

def createoption():
    option = Tk()
    Canevas = Canvas(option,width=800,height=800,bg ='white')
    Canevas.pack()
    Mapolice = Font(family='Liberation Serif', size=100)
    Canevas.create_text(400,50,text="OPTION",fill="red",font=Mapolice)
    Canevas.create_text(100,200,text="MUSIQUE",fill="red",font=Mapolice)
    bouton=Button(option, text="OUI", command=createoption,width=10,height=5,bg='Black',fg='red')
    bouton.place(x=200,y=150)
    bouton=Button(option, text="NON", command=createoption,width=10,height=5,bg='Black',fg='red')
    bouton.place(x=500,y=150)
    Canevas.create_text(100,350,text="Nombre(s) joueur",fill="red",font=Mapolice)
    bouton=Button(option, text="1", command=createoption,width=10,height=5,bg='Black',fg='red')
    bouton.place(x=200,y=300)
    bouton=Button(option, text="2", command=createoption,width=10,height=5,bg='Black',fg='red')
    bouton.place(x=500,y=300)
    
    
##########################################################
##########    Variables ##################################
##########################################################
a=1
b=1
c=1
d1=1
e=1
f=1
h=1
n=0
u=0

compteur=0

couleur=randint(1,2)


#########################################################
########## Interface graphique ##########################
##########################################################

Mafenetre = Tk()
Mafenetre.title("Titre")
Canevas = Canvas(Mafenetre,width=1500,height=1000,bg ='grey')
Canevas.pack()
font = Font(family='Liberation Serif', size=200) # création d'une police pour l'affichage du texte
bouton=Button(Mafenetre, text="Jouer à 2 ", command=pagejeu,bg='red',fg='yellow',width=40,height=5)
bouton.place(x=425,y=500)
bouton=Button(Mafenetre, text="Jouer contre un ordi ", command=iapage,bg='red',fg='yellow',width=40,height=5)
bouton.place(x=725,y=500)

#bouton=Button(Mafenetre, text="OPTION", command=createoption,width=20,height=5,bg='red',fg='yellow')
#bouton.place(x=500,y=700)

bouton=Button(Mafenetre, text="Règles", command=regle,width=25,height=5,bg='red',fg='yellow')
bouton.place(x=600,y=600)

Mapolice = Font(family='Liberation Serif', size=100) # création d'une police pour l'affichage du texte
Canevas.create_text(700,200,text="PUISSANCE 4",fill="red",font=Mapolice)

###########################################################
########### Receptionnaire d'évènement ####################
###########################################################
 
##########################################################
############# Programme principal ########################
##########################################################


###################### FIN ###############################  

Mafenetre.mainloop()